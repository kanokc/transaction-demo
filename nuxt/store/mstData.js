import tnxCollection from './mockup/transaction'

export const state = () => ({
  transaction: [],
  transactionFind: {},
})

export const mutations = {
  SET_CANCELED(state, obj) {
    obj.status = 'Canceled'
    state.transactionFind[obj.payment_id] = obj
  },
  SET_LIST(state) {
    const data = tnxCollection.transaction
    const lookup = {}
    data.forEach((d) => {
      lookup[d.payment_id] = d
    })
    state.transaction = data
    state.transactionFind = lookup
  },
}

export const actions = {
  setCanceled({ commit }, obj) {
    commit('SET_CANCELED', obj)
  },
  init(commit) {
    commit('SET_LIST')
  },
}
