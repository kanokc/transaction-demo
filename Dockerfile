# load official base image of Java Runtime
FROM openjdk:8-jdk-alpine

# Set volume point to /tmp
VOLUME /tmp

EXPOSE 8898

ARG VERSION=#version_app#
ARG JAR_FILE=target/transaction-demo-${VERSION}.jar

# Add jar file to the container
ADD ${JAR_FILE} app.jar

# Start application
ENTRYPOINT ["java", "-jar", "/app.jar"]